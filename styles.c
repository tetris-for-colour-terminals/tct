#include <curses.h>
#include "tt.h"
#include "pieces.h"
#include "styles.h"

struct style styles[FIRST_PIECE_COLOUR + PIECE_COLOUR_COUNT] =
{
  /* Normal text */
  {COLOR_YELLOW, COLOR_BLACK},
  /* Highlight text */
  {COLOR_CYAN, COLOR_BLUE},
  /* Walls */
  {COLOR_CYAN, COLOR_RED},

  /* Piece styles */
  {COLOR_RED, COLOR_BLACK},
  {COLOR_GREEN, COLOR_BLACK},
  {COLOR_CYAN, COLOR_BLACK},
  {COLOR_YELLOW, COLOR_BLACK},
  {COLOR_MAGENTA, COLOR_BLACK},
  {COLOR_WHITE, COLOR_BLACK},
  {COLOR_BLUE, COLOR_BLACK},
  {COLOR_WHITE, COLOR_BLACK},
  {COLOR_RED, COLOR_BLACK},
  {COLOR_GREEN, COLOR_BLACK},
  {COLOR_CYAN, COLOR_BLACK},
  {COLOR_YELLOW, COLOR_BLACK},
};

void init_styles(void)
{
  int i;
  for(i = 0; i < sizeof(styles)/sizeof(styles[0]); i++)
    init_pair(i, styles[i].fg, styles[i].bg);
}
