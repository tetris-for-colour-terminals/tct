extern struct style styles[];

#define NORMAL_TEXT_COLOUR COLOR_PAIR(0)
#define HIGHLIGHT_TEXT_COLOUR COLOR_PAIR(1)
#define WALL_COLOUR COLOR_PAIR(2)
#define FIRST_PIECE_COLOUR 3
#define PIECE_COLOUR_COUNT 12
#define PIECE_COLOUR(piece) COLOR_PAIR(FIRST_PIECE_COLOUR + (&(piece) - pieces) % PIECE_COLOUR_COUNT)

void init_styles(void);
